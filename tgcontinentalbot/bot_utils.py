# pylint: disable=W0613
# mypy: no-strict-optional
"""Commands and conversation handlers of the bot."""
from telegram import Update
from telegram.ext import CallbackContext, ConversationHandler

from tgcontinentalbot.continental import (
    ContinentalGame,
    ContinentalGameNotFoundError,
    ContinentalGameRoundMissmatchError,
)

DEFAULT_STARTING_CARDS: int = 12
"""Default number of cards to start a game."""

PARTICIPANTS, STARTING_CARDS, ADD_ROUND = range(3)
"""Conversation handler steps."""


def cmd_help(update: Update, context: CallbackContext) -> None:
    """Shows help whenever the /help or /start command are invoked."""
    update.message.reply_text(
        "Hello! This is the continental card game bot. Outside the game, there "
        "are three commands available:\n\n/help - show this message\n/new - "
        "create a new Continental game\n/continue - follow up an old game. "
        "\n\nOnce within a game, you can send the following command:\n\n"
        "/finish - this will end the game and show the result. You will be "
        "able to continue the game later or create a new one."
    )


def cmd_new(update: Update, context: CallbackContext) -> int:
    """Starts a new game of Continental."""
    update.message.reply_text(
        "Hello! This is the Continental card game bot. I will take care of "
        "your points during the game.\n\nPlease, first send me the "
        "participants (one name per line). For example:"
    )
    update.message.reply_text(
        "```\nParticipant 1\nParticipant 2\nParticipant 3```",
        parse_mode="MarkdownV2",
    )

    return PARTICIPANTS


def cmd_continue(update: Update, context: CallbackContext) -> int:
    """Continues an already started game of continental."""
    user_id = str(update.message.chat_id)

    if not ContinentalGame.exists_game(user_id):
        update.message.reply_text(
            "There is no game in progress. Please create a new one with /new."
        )

        return ConversationHandler.END

    participants: tuple

    with ContinentalGame(user_id) as continental_game:
        participants = continental_game.participants
        next_round_cards = continental_game.next_round_cards
        context.user_data["participants"] = participants
        context.user_data["next_round_cards"] = next_round_cards
        update.message.reply_text("Current game score:")
        update.message.reply_text(
            f"```\n{continental_game}```", parse_mode="MarkdownV2"
        )

    update.message.reply_text(
        "Whenever you are ready just send the round scores in order ("
        f"{', '.join(participants)}) separated by spaces ({next_round_cards} "
        "cards), for example:"
    )
    update.message.reply_text("`20 100 -10`", parse_mode="MarkdownV2")

    return ADD_ROUND


def select_participants(update: Update, context: CallbackContext) -> int:
    participants = tuple(update.message.text.split("\n"))

    if len(participants) <= 1:
        update.message.reply_text(
            "Sorry, we need at least two participants to play the game. "
            "Remember, write one participant name per line, like this:"
        )
        update.message.reply_text(
            "```\nParticipant 1\nParticipant 2\nParticipant 3```",
            parse_mode="MarkdownV2",
        )

        return PARTICIPANTS

    context.user_data["participants"] = participants
    update.message.reply_text(
        f"Alright, the participants will be: {', '.join(participants)}. Now "
        "tell me the number of cards you are going to deal for the first "
        f"round. If you want to use {DEFAULT_STARTING_CARDS} cards for "
        "starters, you may want to send the /skip command, as it is the "
        "default."
    )

    return STARTING_CARDS


def select_starting_cards(update: Update, context: CallbackContext) -> int:
    try:
        starting_cards = int(update.message.text)
    except ValueError:
        update.message.reply_text(
            f"Error, {update.message.text} is not a valid number of starting "
            "cards for the game. Please, enter a valid integer."
        )
    else:
        context.user_data["next_round_cards"] = starting_cards

    update.message.reply_text(
        f"Selecting {starting_cards} as the numer of cards for the first round."
        "\n\nWhenever you are ready just send the round scores in order "
        "separated by spaces, for instance:"
    )
    update.message.reply_text("`20 100 -10`", parse_mode="MarkdownV2")

    user_id = str(update.message.chat_id)

    if ContinentalGame.exists_game(user_id):
        with ContinentalGame(user_id) as continental_game:
            continental_game.delete_game()

    return ADD_ROUND


def skip_select_starting_cards(
    update: Update, context: CallbackContext
) -> int:
    context.user_data["next_round_cards"] = DEFAULT_STARTING_CARDS
    update.message.reply_text(
        f"Perfect, {DEFAULT_STARTING_CARDS} cards will be used for the first "
        "round.\n\nWhenever you are ready just send the round scores in order "
        "separated by spaces, for example:"
    )
    update.message.reply_text("`20 100 -10`", parse_mode="MarkdownV2")

    user_id = str(update.message.chat_id)

    if ContinentalGame.exists_game(user_id):
        with ContinentalGame(user_id) as continental_game:
            continental_game.delete_game()

    return ADD_ROUND


def add_round(update: Update, context: CallbackContext) -> int:
    try:
        scores = tuple(map(int, update.message.text.split()))
    except ValueError:
        update.message.reply_text(
            "There was an error parsing the scores. Please enter the scores of "
            "each participant respecting the order and divided by a single "
            "space in the same line."
        )

        return ADD_ROUND

    scores_number = len(scores)
    participants_number = len(context.user_data.get("participants"))

    if scores_number != participants_number:
        update.message.reply_text(
            f"There was a problem reading the scores. You gave {scores_number} "
            f"but there are {participants_number} participants. Please enter "
            "the scores of each participant respecting the order and divided "
            "by a single space in the same line."
        )

        return ADD_ROUND

    next_round_cards: int = context.user_data.get(
        "next_round_cards", DEFAULT_STARTING_CARDS
    )
    user_id = str(update.message.chat_id)
    first_round = False

    if not ContinentalGame.exists_game(user_id):
        ContinentalGame.create_game(
            user_id, context.user_data.get("participants")
        )
        first_round = True

    try:
        with ContinentalGame(user_id) as continental_game:
            continental_game.add_round((next_round_cards, *scores))
            if not first_round:
                update.message.reply_text("Current game score:")
                update.message.reply_text(
                    f"```\n{continental_game}```", parse_mode="MarkdownV2"
                )
    except ContinentalGameRoundMissmatchError:
        update.message.reply_text(
            "There was an error parsing the round scores. There must be the "
            "same number of scores than participant for the game."
        )

        return ADD_ROUND

    next_round_cards += 1
    context.user_data["next_round_cards"] = next_round_cards
    update.message.reply_text(
        "Perfect. Now send me the next round scores. Number of cards to deal: "
        f"{next_round_cards}. To finish the game and see the results, just "
        "send the command /finish."
    )

    return ADD_ROUND


def add_round_error(update: Update, context: CallbackContext) -> int:
    update.message.reply_text(
        "There was an error understanding the previous message. Please, send "
        "the scores in the following format:"
    )
    update.message.reply_text("`20 100 -10`", parse_mode="MarkdownV2")

    return ADD_ROUND


def cmd_finish(update: Update, context: CallbackContext) -> int:
    try:
        with ContinentalGame(str(update.message.chat_id)) as continental_game:
            ranking_text = "\n".join(
                f"{rank + 1} - {participant}"
                for rank, participant in enumerate(continental_game.ranking)
            )
            update.message.reply_text(
                "The game has finished. This is the final podium:\n\n"
                f"{ranking_text}"
            )
    except ContinentalGameNotFoundError:
        update.message.reply_text("No game was found.")

    return ConversationHandler.END
