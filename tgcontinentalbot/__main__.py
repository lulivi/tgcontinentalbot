"""Run the bot."""
from os import getenv

from telegram.ext import (
    CommandHandler,
    ConversationHandler,
    Filters,
    MessageHandler,
    Updater,
)

from tgcontinentalbot.bot_utils import (
    ADD_ROUND,
    PARTICIPANTS,
    STARTING_CARDS,
    add_round,
    add_round_error,
    cmd_continue,
    cmd_finish,
    cmd_help,
    cmd_new,
    select_participants,
    select_starting_cards,
    skip_select_starting_cards,
)


def main():
    """Run the bot."""
    # Create the Updater and pass it your bot's token.
    updater = Updater(getenv("BOT_TOKEN"))

    # Get the dispatcher to register handlers
    dispatcher = updater.dispatcher

    # Add conversation handler with the states GENDER, PHOTO, LOCATION and BIO
    conv_handler = ConversationHandler(
        entry_points=[
            CommandHandler("new", cmd_new),
            CommandHandler("continue", cmd_continue),
        ],
        states={
            PARTICIPANTS: [
                CommandHandler("finish", cmd_finish),
                MessageHandler(Filters.text, select_participants),
            ],
            STARTING_CARDS: [
                CommandHandler("skip", skip_select_starting_cards),
                MessageHandler(
                    Filters.regex(r"^[0-9]+$"), select_starting_cards
                ),
            ],
            ADD_ROUND: [
                MessageHandler(Filters.regex(r"^(-?[0-9\s])+$"), add_round),
                MessageHandler(
                    Filters.text & ~Filters.command, add_round_error
                ),
            ],
        },
        fallbacks=[CommandHandler("finish", cmd_finish)],
    )

    dispatcher.add_handler(conv_handler)
    dispatcher.add_handler(CommandHandler("help", cmd_help))
    dispatcher.add_handler(CommandHandler("start", cmd_help))

    # Start the Bot
    updater.start_polling()

    # Run the bot until you press Ctrl-C or the process receives SIGINT,
    # SIGTERM or SIGABRT. This should be used most of the time, since
    # start_polling() is non-blocking and will stop the bot gracefully.
    updater.idle()


if __name__ == "__main__":
    main()
