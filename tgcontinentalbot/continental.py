# pylint: disable=logging-fstring-interpolation
"""Continental game encapsulation."""
import csv
import logging

from pathlib import Path
from typing import List, Optional, Tuple

logging.basicConfig(
    format="[%(asctime)s][%(name)s][%(levelname)s]: %(message)s",
    level=logging.DEBUG,
)
logger = logging.getLogger(__name__)

DEFAULT_GAME_DIR: Path = Path.cwd().joinpath("games")


class ScoreError(Exception):
    """Error related with the score of a game."""


class ScoreMissMatchError(ScoreError, ValueError):
    """The round values does not match with the game columns."""


class Score:
    """Serialization and deserialization of a game file."""

    def __init__(self, file_path: Path) -> None:
        """Reads the score from the given path.

        :param file_path: path to the game file.

        """
        with open(file_path, "r") as file_descriptor:
            reader = csv.reader(file_descriptor)
            self._headers: Tuple[str, ...] = tuple(next(reader))
            self._score: List[List[int]] = []

            for score_round in reader:
                self._score.append(list(map(int, score_round)))

    @property
    def participants(self) -> Tuple[str, ...]:
        """Obtains the list of game participants.

        :returns: a tuple containing the participants.

        """
        return self._headers[1:]

    @property
    def next_round_cards(self) -> int:
        """Obtains the next round cards.

        :returns: number of the next round cards.

        """
        return self._score[-1][0] + 1

    @staticmethod
    def create_score_file(
        file_path: Path, participants: Tuple[str, ...]
    ) -> None:
        """Creates a CSV with the cards column and the participants.

        :param file_path: path to the game file.
        :param participants: the participants of the game.

        """
        with open(file_path, "w") as file_descriptor:
            csv.writer(file_descriptor).writerow(("cards", *participants))

    def add_round(self, round_score: Tuple[int, ...]) -> None:
        """Adds a new round to the game.

        :param round_score: group of the current round scores.

        """
        round_score_cols = len(round_score)

        if round_score_cols != len(self._headers) or (
            self._score and len(self._score[0]) != round_score_cols
        ):
            raise ScoreMissMatchError(
                "The argument round columns missmatches from the game columns: "
                f"'{tuple(self._headers)}' vs '{round_score}'"
            )

        if any(map(lambda x: not isinstance(x, int), round_score)):
            raise ScoreMissMatchError(
                f"The round scores are not integers: {round_score}"
            )

        self._score.append(list(round_score))

    def save(self, file_path: Path) -> None:
        """Serialises the scores into the given file path.

        :param file_path: path to the game file.

        """
        with open(file_path, "w") as file_descriptor:
            writer = csv.writer(file_descriptor)
            writer.writerow(self._headers)
            writer.writerows(self._score)

    def _score_sum(self) -> List[int]:
        """Obtain the total of each column.

        :returns: a list containing the sum of each column of the score.

        """
        return [sum(col) for col in zip(*self._score)]

    @property
    def ranking(self) -> List[str]:
        """Obtains the game rankings.

        :returns: A list containing the participants sorted by points.

        """
        score_sum = self._score_sum()[1:]
        sorted_score_sum = sorted(score_sum)
        ranks = [sorted_score_sum.index(item) for item in score_sum]
        rank_index = {i: j for i, j in enumerate(ranks)}
        participants = self.participants
        return sorted(
            participants,
            key=lambda key: rank_index[participants.index(key)],
        )

    def __str__(self):
        """Pretty printing of the score.

        :returns: the string form of the current results.

        """
        table_format = "{:^8}" * len(self._headers)
        score_str = table_format.format("Cards", *self._headers[1:])

        for score_round in self._score:
            score_str += f"\n{table_format.format(*score_round)}"

        score_sum = self._score_sum()
        score_sum[0] = "Total"
        score_str += f"\n\n{table_format.format(*score_sum)}"

        return score_str


class ContinentalGameError(Exception):
    """Base class for all the errors related to a Continental game."""


class ContinentalGameNotFoundError(ContinentalGameError, FileNotFoundError):
    """The game file could not be found."""


class ContinentalGameRoundMissmatchError(ContinentalGameError, ValueError):
    """The round values does not match with the game columns."""


class ContinentalGame:
    """Continental card game."""

    def __init__(self, game_id: str) -> None:
        """Creates a new continental game.

        This must be used along with context management, in order for it to
        work properly.

        :param game_id: unique identifier of the game.
        :param game_dir_path: base directory in which the game file will be
            created.

        """
        self._game_file_path = ContinentalGame._get_game_path_from_id(game_id)
        self.__score: Optional[Score] = None
        self._finalise_game = False

    @staticmethod
    def _get_game_path_from_id(game_id: str) -> Path:
        return DEFAULT_GAME_DIR.joinpath(f"{game_id}.csv")

    @property
    def _score(self) -> Score:
        """Get the game score.

        :returns: a Pandas DataFrame object with the game score.

        """
        if self.__score is None:
            raise ValueError("The score is not initialized")

        return self.__score

    @_score.setter
    def _score(self, value: Score) -> None:
        """Set the value of the score.

        :param value: the new variable contents.

        """
        self.__score = value

    @_score.deleter
    def _score(self) -> None:
        """Deletes the score attribute."""
        self.__score = None

    @property
    def participants(self) -> Tuple[str, ...]:
        """Obtains the list of game participants.

        :returns: a tuple containing the participants.

        """
        return self._score.participants

    @property
    def next_round_cards(self) -> int:
        """Obtains the next round cards.

        :returns: number of the next round cards.

        """
        return self._score.next_round_cards

    @property
    def ranking(self) -> List[str]:
        """Obtains the game rankings.

        :returns: A list containing the participants sorted by points.

        """
        return self._score.ranking

    @staticmethod
    def exists_game(game_id: str):
        """Check if the csv file of a game exists.

        :param game_id: identificator of the game.
        :returns: True if the file game exists. False otherwise.

        """
        return ContinentalGame._get_game_path_from_id(game_id).exists()

    @staticmethod
    def create_game(
        game_id: str, participants: Tuple[str], replace=False
    ) -> None:
        """Creates a new save file for the new game.

        :param game_id: identificator of the game.
        :param participants: a tuple with the participants of the new game.
        :param replace: if True this method will replace the save file if found.

        """
        DEFAULT_GAME_DIR.mkdir(exist_ok=True)
        game_file_path = ContinentalGame._get_game_path_from_id(game_id)
        logger.debug(f"Creating a new game file: {game_file_path}.")

        if game_file_path.exists() and not replace:
            logger.warning(
                "Game file already exists. Use the replace option in order to "
                "overwrite it."
            )
            return

        Score.create_score_file(game_file_path, participants)

    def add_round(self, round_score: Tuple[int, ...]) -> None:
        """Add a new round of scores.

        :param round_score: a tuple with the number of cards and each
            participant score.
        :raises ContinentalGameRoundMissmatchError: if the number of items does
            not match with the number of csv columns or if any of the items in
            the tuple is not a integer.

        """
        logger.debug(f"Adding a new round to the game: {round_score}.")
        try:
            self._score.add_round(round_score)
        except ScoreMissMatchError as err:
            logger.error(err)
            raise ContinentalGameRoundMissmatchError(
                "There was a missmatch between the round score and the "
                "participants or the score type"
            ) from err

    def load_game(self) -> None:
        """Load a game from its save file.

        :raises ContinentalGameNotFoundError: if there are errors locating the
            file

        """
        logger.debug(f"Loading game from file {self._game_file_path}")
        try:
            self._score = Score(self._game_file_path)
        except FileNotFoundError as err:
            logger.error("Error loading game. Game file not found.")
            raise ContinentalGameNotFoundError(
                f"Could not find the game file: {str(self._game_file_path)}"
            ) from err

    def save_game(self) -> None:
        """Same the game state into its csv file."""
        logger.debug(f"Saving game to file {self._game_file_path}.")
        self._score.save(self._game_file_path)

    @staticmethod
    def delete_game_file(user_id: str) -> None:
        """Deletes a game file.

        :param file_path: path to the game file.

        """
        ContinentalGame._get_game_path_from_id(user_id).unlink(missing_ok=True)

    def delete_game(self) -> None:
        """Delete the game file and activate the :attr:`_finalise_game` flag.

        This way the `with` block exit will not save the last score and recreate
        the state file.

        """
        logger.debug(f"Deleting finalised game file: {self._game_file_path}")
        ContinentalGame.delete_game_file(self._game_file_path.stem)
        del self._score

    def __enter__(self) -> "ContinentalGame":
        """Load a game and return the object when in a `with` statement."""
        self.load_game()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb) -> None:
        """Save game and unload the score after the with block.

        If we are finalising a game (check :func:`delete_game`) we will not have
        anything to save, so we scape the method.

        """
        if self.__score:
            self.save_game()
            del self._score

    def __str__(self) -> str:
        """Print the score in a readable way.

        :returns: the string of the results.

        """
        return str(self._score)
